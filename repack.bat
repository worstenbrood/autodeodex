IF "%1"=="" GOTO QUIT
IF "%2"=="" GOTO QUIT
ECHO [+] Repacking %~n1%~x1 ...
MD "%~dp0temp_%~n1" 2>NUL 1>&2
"%~dp0%TOOLS_DIR%\7za.exe" x -y -o"%~dp0temp_%~n1" "%1" 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
FOR /F "tokens=*" %%F IN ('DIR /B /S "%~dp0temp_%~n1\*.PNG"') DO (
	"%~dp0%TOOLS_DIR%\optipng.exe" -o7 -quiet "%%F"
)
"%~dp0%TOOLS_DIR%\7za.exe" a -y -tzip "%~dp0%DEODEX_PRE%%~n2\%~n1%~x1" "%~dp0temp_%~n1\*" -mx%COMPRESSIONLEVEL% 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
:NOERROR
ECHO [+] Succesfully repacked %~n1%~x1 !
GOTO QUIT
:ERROR
ECHO [%TIME:~0,2%:%TIME:~3,2% %DATE:~3,2%/%DATE:~6,2%/%DATE:~9,4%] Repack Failed: %1 >> "%~dp0error.log"
ECHO [-] Error while repacking %~n1%~x1 !
:QUIT
RD /s /q "%~dp0temp_%~n1" 2>NUL 1>&2