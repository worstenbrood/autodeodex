IF "%1"=="" GOTO QUIT
IF "%2"=="" GOTO QUIT
IF NOT EXIST "%~dp0%~n2\%~n1.odex" GOTO MOVE
ECHO [+] Deodexing %~n1%~x1 ...
MD "%~dp0temp_%~n1" 2>NUL 1>&2
"%~dp0%TOOLS_DIR%\7za.exe" x -y -o"%~dp0temp_%~n1" "%1" 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
IF "%IGNORE%"=="0" (
	java -Xmx512m -jar "%~dp0%TOOLS_DIR%\%BAKSMALI%" -a %SDK_VERSION% -d "%~dp0%FRAMEWORK_DIR%" -d "%~dp0%DEODEX_PRE%%FRAMEWORK_DIR%" -c "%BOOTCLASSPATH%" -x "%~dp0%~n2\%~n1.odex" -o "%~dp0out"
) ELSE (
	java -Xmx512m -jar "%~dp0%TOOLS_DIR%\%BAKSMALI%" -a %SDK_VERSION% -I -d "%~dp0%FRAMEWORK_DIR%" -d "%~dp0%DEODEX_PRE%%FRAMEWORK_DIR%" -c "%BOOTCLASSPATH%" -x "%~dp0%~n2\%~n1.odex" -o "%~dp0out" 
)
IF "%ERRORLEVEL%"=="1" GOTO ERROR
java -jar "%~dp0%TOOLS_DIR%\%SMALI%" -a 13 "%~dp0out" -o "%~dp0temp_%~n1\classes.dex"
IF "%ERRORLEVEL%"=="1" GOTO ERROR
"%~dp0%TOOLS_DIR%\7za.exe" a -y -tzip "%~dp0%DEODEX_PRE%%~n2\%~n1%~x1" "%~dp0temp_%~n1\*" -mx%COMPRESSIONLEVEL% 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
GOTO NOERROR
:MOVE
MD "%~dp0%DEODEX_PRE%%~n2" 2>NUL 1>&2
MOVE /Y "%1" "%~dp0%DEODEX_PRE%%~n2" 2>NUL 1>&2
GOTO QUIT
:ERROR
ECHO [%TIME:~0,2%:%TIME:~3,2% %DATE:~3,2%/%DATE:~6,2%/%DATE:~9,4%] Deodex Failed: %1 >> "%~dp0error.log"
ECHO [-] Error while deodexing %~n1%~x1 !
RD /s /q "%~dp0temp_%~n1" 2>NUL 1>&2
RD /s /q "%~dp0out" 2>NUL 1>&2
GOTO QUIT
:NOERROR
ECHO [+] Succesfully deodexed %~n1%~x1 !
RD /s /q "%~dp0temp_%~n1" 2>NUL 1>&2
RD /s /q "%~dp0out" 2>NUL 1>&2
DEL /q "%~dp0%~n2\%~n1.odex" 2>NUL 1>&2
DEL /q "%~dp0%~n2\%~n1%~x1" 2>NUL 1>&2
:QUIT
