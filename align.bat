IF "%1"=="" GOTO QUIT
ECHO [+] Zipaligning %~n1%~x1 ...
MD "%~dp0temp_%~n1" 2>NUL 1>&2
"%~dp0%TOOLS_DIR%\zipalign.exe" -f 4 "%1" "%~dp0temp_%~n1\%~n1%~x1"
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
MOVE /Y "%~dp0temp_%~n1\%~n1%~x1" "%1" 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
GOTO NOERROR
:ERROR
ECHO  [%TIME:~0,2%:%TIME:~3,2% %DATE:~3,2%/%DATE:~6,2%/%DATE:~9,4%] Zipalign Failed: %1 >> "%~dp0error.log"
ECHO [-] Error while zipaligning %~n1%~x1 !
GOTO QUIT
:NOERROR
ECHO [+] Succesfully zipaligned %~n1%~x1 !
:QUIT
RD /s /q "%~dp0temp_%~n1" 2>NUL 1>&2