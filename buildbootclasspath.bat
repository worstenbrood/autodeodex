IF "%1"=="" GOTO QUIT
FOR /F %%A IN (CUSTOM_CLASSPATH.TXT) DO (
	IF "%~n1"=="%%A" (
		ECHO + Found: %~n1.jar
		SET BOOTCLASSPATH=%BOOTCLASSPATH%:%~n1.jar
	)
)
:QUIT