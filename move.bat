IF "%1"=="" GOTO QUIT
IF "%2"=="" GOTO QUIT
ECHO [+] Moving %~n1%~x1 ...
MOVE /Y "%1" "%2" 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
GOTO NOERROR
:ERROR
ECHO [%TIME:~0,2%:%TIME:~3,2% %DATE:~3,2%/%DATE:~6,2%/%DATE:~9,4%] Move Failed: %1 >> "%~dp0error.log"
ECHO [-] Moving %~n1%~x1 failed (Error:%ERRORLEVEL%) !
GOTO QUIT
:NOERROR
ECHO [+] Succesfully moved %~n1%~x1 !
:QUIT