IF "%1"=="" GOTO QUIT
ECHO [+] Signing %~n1%~x1 ...
MD "%~dp0temp_%~n1" 2>NUL 1>&2
java -jar "%~dp0%TOOLS_DIR%\signapk.jar" "%~dp0%TOOLS_DIR%\testkey.x509.pem" "%~dp0%TOOLS_DIR%\testkey.pk8" "%1" "%~dp0temp_%~n1\%~n1%~x1"
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
MOVE /Y "%~dp0temp_%~n1\%~n1%~x1" "%1" 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
GOTO NOERROR
:ERROR
ECHO [%TIME:~0,2%:%TIME:~3,2% %DATE:~3,2%/%DATE:~6,2%/%DATE:~9,4%] Sign Failed: %1 >> "%~dp0error.log"
ECHO [-] Error while signing %~n1%~x1 !
GOTO QUIT
:NOERROR
ECHO [+] Succesfully signed %~n1%~x1 !
:QUIT
RD /s /q "%~dp0temp_%~n1" 2>NUL 1>&2